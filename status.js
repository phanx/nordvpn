import chalk from "chalk"

import { getServerInfo } from "./api.js"
import { listCities, listCountries } from "./list.js"

export const normalizeCity = (city, cities) => {
	if (typeof city !== "string" || city.length < 2) return
	if (!Array.isArray(cities)) return

	let match = cities.find(c => c.name === city)
	if (match) return match.name

	const cityLower = city.toLowerCase().replace(/[^a-z]/g, "")
	match = cities.find(c => c.name.toLowerCase().replace(/[^a-z]/g, "") === cityLower)
	if (match) return match.name
}

export const normalizeCountry = (country, countries) => {
	if (typeof country !== "string" || country.length < 2) return
	if (!Array.isArray(countries)) return

	let match

	if (country.length === 2) {
		country = country.toLowerCase()
		match = countries.find(c => c.code === country)
		if (match) return match.name
	}

	match = countries.find(c => c.name === country)
	if (match) return match.name

	const countryLower = country.toLowerCase().replace(/[^a-z]/g, "")
	match = countries.find(c => c.name.toLowerCase().replace(/[^a-z]/g, "") === countryLower)
	if (match) return match.name
}

export const getBestServer = async (country, city, multi) => {
	const { cities, countries, servers } = await getServerInfo()

	if (city) {
		city = normalizeCity(city, cities)
		if (!city) {
			return "INVALID_CITY"
		}
	}

	if (country) {
		country = normalizeCountry(country, countries)
	}

	if (!country && !city) {
		return "INVALID_COUNTRY"
	}

	const matches = city
		? servers.filter(s => s.city === city)
		: servers.filter(s => s.country === country)

	if (matches.length === 0) {
		return "NO_MATCH"
	}

	if (multi) {
		let best = matches.sort((a, b) => a.load < b.load ? -1 : 1)
		if (typeof multi === "number") {
			best = best.slice(0, Math.floor(multi))
		}
		return best
	}

	const best = matches.reduce((a, b) => a.load < b.load ? a : b)
	return best
}

export const printSingleStatus = (shortname, name, load) => {
	const color = load > 65 ? "red" : load > 35 ? "yellow" : "green"
	const cload = chalk[color](`${load}%`)
	const sep = chalk.gray("-")
	console.log(`   ${chalk.cyan(shortname)} ${sep} ${name} ${sep} ${cload}`)
}

export const printStatus = async (country, city, quiet, multi) => {
	const best = await getBestServer(country, city, multi)
	if (typeof best === "string") {
		switch (best) {
			case "INVALID_COUNTRY":
				if (!quiet) await listCountries()
				return
			case "INVALID_CITY":
				if (!quiet) await listCities()
				return
			case "NO_MATCH":
				if (!quiet) console.log("No servers matching", country, city)
				return
			default:
				if (!quiet) console.log("Unknown error", best)
				return
		}
	}

	if (Array.isArray(best)) {
		if (quiet) {
			best.forEach(server => {
				console.log(server.domain.split(".")[0])
			})
		} else {
			best.forEach(server => {
				printSingleStatus(server.domain.split(".")[0], server.name, server.load)
			})
		}
		return
	}

	const shortname = best.domain.split(".")[0]
	if (quiet) {
		console.log(shortname)
		return
	}
	printSingleStatus(shortname, best.name, best.load)
}
