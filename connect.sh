#!/usr/bin/env bash

APPDIR="$(dirname $(readlink -f $0))"
ALERTICON="${APPDIR}/assets/icon.png"
ALERTSOUND="${APPDIR}/assets/alert.wav"

if [ -p /dev/stdin ]; then
	# accept piped input
	while IFS= read line; do
		SERVER="${line}"
	done
else
	SERVER="$1"
fi

if [ -z "$SERVER" ]; then
	echo "Usage: nord <server>"
	exit 1
fi

if [ -z "$XDG_DATA_HOME" ]; then
	XDG_DATA_HOME="${HOME}/.local/share"
fi
CONFIG="${XDG_DATA_HOME}/nordvpn/servers/${SERVER}.nordvpn.com.udp.ovpn"

if [ ! -f "$CONFIG" ]; then
	echo "No matching configuration file found."
	exit 1
fi

if [ -z "$XDG_CONFIG_HOME" ]; then
	XDG_CONFIG_HOME="${HOME}/.config"
fi
AUTH_DIR="${XDG_CONFIG_HOME}/nordvpn"
AUTH="${AUTH_DIR}/auth"

if [ ! -f "$AUTH" ]; then
	echo "Enter NordVPN account name: "
	read user
	if [ -z "$user" ]; then
		echo "Account name is required!"
		exit 1
	fi

	echo "Enter NordVPN password: "
	read pass
	if [ -z "$pass" ]; then
		echo "Password is required!"
		exit 1
	fi

	if [ ! -d "$AUTH_DIR" ]; then
		mkdir -p "$AUTH_DIR"
	fi

	echo "$user" > "$AUTH"
	echo "$pass" >> "$AUTH"
	chmod 600 "$AUTH"
fi

echo -ne "\033]0;${SERVER} - NordVPN\007"

sudo openvpn \
	--config "${CONFIG}" \
	--auth-user-pass "${AUTH}" \
	--mute-replay-warnings \
	--script-security 2 \
	--up /etc/openvpn/update-resolv-conf \
	--down /etc/openvpn/update-resolv-conf \
	--down-pre

pkill qbittorrent
pkill transmission
pkill utorrent

paplay "$ALERTSOUND"

notify-send -u critical -t 30000 -i "$ALERTICON" \
	"VPN Disconnected" \
	"The connection to NordVPN (${SERVER}) has terminated."
