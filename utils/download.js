import fs from "fs"
import https from "https"

const download = (url, dest) => {
	return new Promise((resolve, reject) => {
		const req = https.get(url, (res) => {
			if (res.statusCode !== 200) {
				return reject(new Error(`Request failed with status code ${ res.statusCode }`))
			}

			const file = fs.createWriteStream(dest)
			file.on("error", (err) => reject(err))
			file.on("finish", () => file.close(() => resolve()))
			res.pipe(file)
		})

		req.on("error", (err) => reject(err))
	})
}

export default download
