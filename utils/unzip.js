import fs from "fs"
import path from "path"
import yauzl from "yauzl"

const OPTIONS = {
	lazyEntries: true
}

const openzip = (filename) => new Promise((resolve, reject) => {
	yauzl.open(filename, OPTIONS, (err, zipfile) => {
		if (err) reject(err)
		else resolve(zipfile)
	})
})

const unzip = (zipname, dest) => new Promise((resolve, reject) => {
	openzip(zipname).then(zipfile => {
		zipfile.readEntry()
		zipfile.on("entry", (entry) => {
			if (/\/$/.test(entry.fileName)) {
				// directory
				return zipfile.readEntry()
			}

			const fileName = entry.fileName.replace(/.+\//, "")
			if (!/^[a-z]{2}\d+\..+\.udp\.ovpn$/.test(fileName)) {
				// do not want
				return zipfile.readEntry()
			}

			zipfile.openReadStream(entry, (err, stream) => {
				if (err) throw err

				const file = fs.createWriteStream(path.join(dest, fileName))
				stream.pipe(file)

				file.on("error", (err) => reject(err))
				file.on("finish", () => {
					file.close()
					zipfile.readEntry()
				})
			})
		})

		zipfile.on("end", () => resolve())
	})
})

export default unzip
