import os from "os"
import path from "path"

let cacheDir
let configDir
let dataDir
if (process.platform == "darwin") {
	cacheDir = path.join(os.homedir(), "Library", "Caches", "NordVPN")
	configDir = path.join(os.homedir(), "Library", "Application Support", "NordVPN")
	dataDir = configDir
} else if (process.platform == "win32") {
	cacheDir = path.join(process.env.LOCALAPPDATA || os.homedir(), "NordVPN") // TODO where should it go?
	configDir = path.join(process.env.APPDATA || os.homedir(), "NordVPN")
	dataDir = path.join(process.env.LOCALAPPDATA || os.homedir(), "NordVPN")
} else {
	cacheDir = path.join(process.env.XDG_CACHE_HOME || path.join(os.homedir(), ".cache"), "nordvpn")
	configDir = path.join(process.env.XDG_CONFIG_HOME || path.join(os.homedir(), ".config"), "nordvpn")
	dataDir = path.join(process.env.XDG_DATA_HOME || path.join(os.homedir(), ".local", "share"), "nordvpn")
}

const config = {
	dataFile: path.join(cacheDir, "servers.json"),
	overridesFile:  path.join(configDir, "overrides.json"),
	serversDir: path.join(dataDir, "servers"),
	updateInterval: 1000 * 60 * 5, // 5 minutes
	urls: {
		download: "https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip",
		servers: "https://api.nordvpn.com/v2/servers?limit=16384",
	},
}

export default config
