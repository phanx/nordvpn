# NordVPN CLI for Linux

Unofficial CLI helper for NordVPN users on Linux.

## Usage

- `nordvpn <country>` - Get best server for the specified country
- `nordvpn -c <city>` - Get best server for the specified US city
- `nordvpn -l` - List available countries
- `nordvpn -lc` - List available cities in the United States
- `nordpvn -u` - Download updated OpenVPN config files from NordVPN

Use the `-q` flag with `<country>` or `-c <city>` to print only the
server shortname (e.g. `us123`), suitable for piping to other programs.

Country names are not case-sensitive and can be specified as a 2-letter
ISO code (e.g. `us`) or as a full name, optionally with spaces removed
or underscored (e.g. `"United States"`, `united_states`, `unitedstates`).

City names are treated the same, except they do not have 2-letter codes.

Config files are stored in `${HOME}/.config/nordvpn/servers`.

## TODO

- Configuration options that live independently of the script, e.g.
  default city/country, list of processes to kill, etc.

- Actually connect to a server and monitor the connection, **maybe**.
  Currently I just do `nordvpn -c miami -q | nordvpn-connect`, which
  is the included `connect.sh` symlinked into a location in my PATH.

## Installation

1. `git clone $URL $DIR` or download the ZIP file
2. `cd $DIR`
3. `npm install`
4. `npm link` to get it in your PATH
5. `ln -s $DIR/connect.sh ~/.local/bin/nordvpn-connect` (optional)

Requires [Node.js](https://nodejs.org).

## Notes

This was originally written long before NordVPN released their official
CLI app, but continues to have the following advantages for me:

- It keeps the OpenVPN process in the foreground for easy monitoring.
- It kills specific processes on disconnect.
- It plays an annoying sound on disconnect.

It also has the following **intentional** limitations:

- It only supports OpenVPN UDP connections.
- It does not have a system-wide kill-switch feature.
- It does not integrate with the Network Manager or other tools.

## License

This software is released under the terms of the **zlib License**.
See the included `LICENSE.txt` file for the full license terms.
