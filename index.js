#!/usr/bin/env node

import fs from "node:fs"
import path from "node:path"

import help from "./help.js"
import { listCities, listCountries } from "./list.js"
import { printStatus } from "./status.js"
import update from "./update.js"

const __dirname = path.dirname(new URL(import.meta.url).pathname)
const pkgmeta = JSON.parse(fs.readFileSync(path.join(__dirname, "package.json")).toString())

const args = { action: "get-country" }

const aliases = {
	"a": "all",
	"c": "city",
	"h": "help",
	"l": "list",
	"q": "quiet",
	"u": "update",
	"v": "version",
}

const handleVerb = (arg) => {
	switch (arg.toLowerCase()) {
		case "all":
			args.multi = true
			return true
		case "city":
			args.action = args.action === "list-countries" ? "list-cities" : "get-city"
			return true
		case "help":
			args.action = "help"
			return true
		case "list":
			args.action = args.action === "get-city" ? "list-cities" : "list-countries"
			return true
		case "quiet":
			args.quiet = true
			return true
		case "update":
			args.action = "update"
			return true
		case "version":
			args.action = "version"
			return true
		default:
			return false
	}
}

const badargs = []

process.argv.slice(2).forEach(arg => {
	if (/^-[1-9][0-9]*$/.test(arg)) {
		args.multi = Number(arg.slice(1))
	}
	else if (/^-[a-z]+$/.test(arg)) {
		arg.slice(1).split("").forEach(letter => {
			const word = aliases[letter]
			const handled = word && handleVerb(word)
			if (!handled) {
				badargs.push(`-${letter}`)
			}
		})
	}
	else if (/^--[a-z]+$/.test(arg)) {
		const handled = handleVerb(arg.slice(2))
		if (!handled) {
			badargs.push(arg)
		}
	} else {
		args[args.action === "get-city" ? "city" : "country"] = arg
	}
})

const main = () => {
	switch (args.action) {
		case "help":
			return help()
		case "update":
			return update()
		case "list-cities":
			return listCities()
		case "list-countries":
			return listCountries()
		case "get-city":
			return printStatus("United States", args.city, args.quiet, args.multi)
		default:
			return printStatus(args.country, null, args.quiet, args.multi)
	}
}

main()
