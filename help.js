const help = () => {
	console.log("Usage: nordvpn [options] command")
	console.log("Available commands:")
	console.log("   <name>     Get best server for country, or city with -c flag")
	console.log("   -5         Get best 5 servers instead of just one")
	console.log("   -c         Consider cities instead of countries")
	console.log("   -l         List available countries, or cities with -c flag")
	console.log("   -u         Update OpenVPN config files")
	console.log("   -h         Display this usage information")
}

export default help
