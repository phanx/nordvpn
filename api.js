import fs from "node:fs"
import path from "node:path"

import config from "./config.js"

const allowedGroups = { "P2P": true, "Standard VPN servers": true }
const allowedServices = { "vpn": true }
const allowedTechnologies = { "openvpn_udp": true }

const byName = (a, b) => a.name.localeCompare(b.name)

const applyOverrides = (data) => {
	if (fs.existsSync(config.overridesFile)) {
		const overrides = JSON.parse(fs.readFileSync(config.overridesFile, "utf8"))
		if (overrides && data.servers) {
			Object.keys(overrides).forEach(shortname => {
				const domain = `${shortname}.nordvpn.com`
				const index = data.servers.findIndex(s => s.domain === domain)
				if (index > -1) {
					data.servers[index] = { ...data.servers[index], ...overrides[shortname] }
				}
			})
		}
	}
	return data
}

export const getServerInfo = async () => {
	if (fs.existsSync(config.dataFile)) {
		const data = JSON.parse(fs.readFileSync(config.dataFile, "utf8"))
		if (data.updated && new Date() - new Date(data.updated) < config.updateInterval) {
			return applyOverrides(data)
		}
	}

	const res = await fetch(config.urls.servers)
	const json = await res.json()
	// const json = JSON.parse(fs.readFileSync("/home/phanx/dev/nordvpn/servers.json", "utf8"))

	const P2P_GROUP_ID = json.groups.find(g => g.title === "P2P").id

	const allowedGroupIds = json.groups.filter(g => allowedGroups[g.title]).map(g => g.id)
	const allowedServiceIds = json.services.filter(s => allowedServices[s.identifier]).map(s => s.id)
	const allowedTechnologyIds = json.technologies.filter(t => allowedTechnologies[t.identifier]).map(t => t.id)

	const locations = json.locations.reduce((t, location) => {
		t[location.id] = location
		return t
	}, {})

	let cities = {}
	let countries = {}
	let servers = []

	json.servers.forEach(server => {
		if (server.status !== "online") return
		if (!server.group_ids || !server.group_ids.some(gid => allowedGroupIds.includes(gid))) return
		if (!server.service_ids || !server.service_ids.some(sid => allowedServiceIds.includes(sid))) return
		if (!server.technologies || !server.technologies.some(t => allowedTechnologyIds.includes(t.id))) return

		const location = server.location_ids && server.location_ids[0] && locations[server.location_ids[0]]
		if (!location) return

		const { latitude, longitude, country } = location
		const { city } = country

		if (!countries[country]) {
			countries[country.name] = {
				id: country.id,
				code: country.code.toLowerCase(),
				name: country.name,
			}
		}

		if (server.group_ids.includes(P2P_GROUP_ID)) {
			countries[country.name].p2p = true
		}

		if (!cities[city.name]) {
			cities[city.name] = {
				id: city.id,
				name: city.name,
				country: country.name,
				latitude,
				longitude,
			}
		}

		servers.push({
			id: server.id,
			name: server.name,
			domain: server.hostname,
			ip: server.station,
			country: country.name,
			city: city.name,
			load: server.load,
		})
	})

	cities = Object.values(cities).sort(byName)
	countries = Object.values(countries).sort(byName)

	const CACHED_DATA_DIR = path.dirname(config.dataFile)
	if (!fs.existsSync(CACHED_DATA_DIR)) {
		fs.mkdirSync(CACHED_DATA_DIR, { mode: 0o750, recursive: true })
	}

	fs.writeFileSync(config.dataFile, JSON.stringify({
		cities,
		countries,
		servers,
		updated: new Date().toISOString(),
	}), "utf8")

	return applyOverrides({ cities, countries, servers })
}
