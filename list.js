import chalk from "chalk"
import columns from "cli-columns"

import { getServerInfo } from "./api.js"

const OPTIONS = { padding: 2, sort: false }

const removeDuplicates = (v, i, t) => t.indexOf(v) === i

////////////////////////////////////////////////////////////////

export const listCities = async () => {
	const { cities, servers } = await getServerInfo()

	const cityList = cities
		.filter(city => city.country === "United States")
		.map(city => {
			const name = city.name
			const count = servers.filter(server => server.city === name).length
			if (count > 0) {
				return `   ${name} ${chalk.gray(`(${count})`)}`
			}
		})
		.filter(v => !!v)

	console.log("Available US cities:")
	console.log(columns(cityList, OPTIONS))
}

////////////////////////////////////////////////////////////////

export const listCountries = async () => {
	const { countries, servers } = await getServerInfo()

	const countryList = countries
		.map(country => {
			const name = country.name
			const count = servers.filter(server => server.country === name).length
			if (count > 0) {
				const code = chalk.cyan(country.code)
				const flag = country.p2p ? chalk.green(" P2P") : ""
				return `   ${code} ${name}${flag} ${chalk.gray(`(${count})`)}`
			}
		})
		.filter(v => !!v)

	console.log("Available countries:")
	console.log(columns(countryList, OPTIONS))
}
