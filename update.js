import fs from "node:fs"
import os from "node:os"
import path from "node:path"

import config from "./config.js"
import download from "./utils/download.js"
import unzip from "./utils/unzip.js"

const serversDir = config.serversDir
const url = config.urls.download

const update = async () => {
	try {
		const zip = path.join(os.tmpdir(), "nord.zip")
		await download(url, zip)

		if (fs.existsSync(serversDir)) {
			fs.rmSync(serversDir, { force: true, recursive: true })
		}
		fs.mkdirSync(serversDir, { mode: 0o750, recursive: true })

		await unzip(zip, serversDir)
		fs.unlinkSync(zip)

		console.log("Done!")
	} catch (err) {
		console.error(err)
	}
}

export default update
